#include "linear.h"

#include <bitset>

// Uses the Grain LSFR as self-shrinking generator to create pseudorandom bits
bool getrandbit () {
  static std::bitset<80> state; //Keeps the 80 bit LSFR state
  bool tmp = 0;
  //If state has not been initialized yet
  if (state.none ()) {
    state.set (); //Initialize with all bits set
    //Throw the first 160 bits away
    for (unsigned i = 0; i < 160; ++i) {
      //Update the state
      tmp =  state[0] ^ state[13] ^ state[23] ^ state[38] ^ state[51] ^ state[62];
      state >>= 1;
      state[79] = tmp;
    }
  }
  //choice records whether the first bit is 1 or 0.
  //The second bit is produced if the first bit is 1.
  bool choice = false;
  do {
    //Update the state
    tmp =  state[0] ^ state[13] ^ state[23] ^ state[38] ^ state[51] ^ state[62];
    state >>= 1;
    state[79] = tmp;
    choice = tmp;
    tmp =  state[0] ^ state[13] ^ state[23] ^ state[38] ^ state[51] ^ state[62];
    state >>= 1;
    state[79] = tmp;
  } while (!choice);
  return tmp;
}

mzd_t *mzd_sample_lmatrix(rci_t n) {
  mzd_t *A = mzd_init(n,n);
  mzd_t *B = mzd_init(n,n);
  do {
    for(rci_t i=0; i<n; i++) {
      for(rci_t j=0; j<n; j++)
        mzd_write_bit(A, n-i-1, n-j-1, getrandbit());
      //mzd_xor_bits(A, n-i-1, n-i-1, 1, 1);
    }
    mzd_copy(B, A);
  } while(mzd_echelonize(A, 0) != n);
  mzd_free(A);
  return B;
};

mzd_t *mzd_sample_kmatrix(rci_t n, rci_t k) {
  mzd_t *A = mzd_init(n, k);
  mzd_t *B = mzd_init(n, k);

  rci_t r = (n<k) ? n : k;

  do {
    for(rci_t i=0; i<n; i++) {
      for(rci_t j=0; j<k; j++)
        mzd_write_bit(A, n-i-1, k-j-1, getrandbit());
      mzd_xor_bits(A, n-i-1, (k+i+1)%k, 1, 1);
    }
    mzd_copy(B, A);
  } while(mzd_echelonize(A, 0) != r);
  mzd_free(A);
  return B;
};


mzd_t *mzd_init_random_vector(rci_t n) {
  mzd_t *A = mzd_init(n,1);
  for(rci_t i=0; i<n; i++)
    mzd_write_bit(A, n-i-1, 0, getrandbit());
  return A;
}


