#include <timing.h>
#include <FHE.h>
#include <EncryptedArray.h>
#include <unistd.h>
#include <fstream>
#include <sstream>
#include <sys/time.h>

#include "args.h"
#include "lowmc.h"
#include "linear.h"
#include "sbox.h"


// class GF2Matrix : public  PlaintextMatrixInterface<PA_GF2> {
// public:
//   PA_INJECT(PA_GF2)

// private:
//   const EncryptedArray& ea;
//   mzd_t *A;

// public:
//   ~GF2Matrix() { mzd_free(this->A); }

//   GF2Matrix(const EncryptedArray& _ea, mzd_t *A) : ea(_ea) {
//     mzd_t *A_T = mzd_transpose(NULL, A);
//     size_t n = ea.size();

//     A = mzd_init(n, n);
//     mzd_set_ui(A, 1);
//     mzd_t *S = mzd_init_window(A, 0, 0, A_T->nrows, A_T->ncols);
//     mzd_copy(S, A_T);
//     mzd_free(S);
//     mzd_free(A_T);

//     this->A = A;
//   }

//   virtual const EncryptedArray& getEA() const {
//     return ea;
//   }

//   virtual void get(RX& out, long i, long j) const {
//     assert(i >= 0 && i < ea.size());
//     assert(j >= 0 && j < ea.size());
//     out = GF2X(0, mzd_read_bit(A, i, j));
//   }
// };

void sbox_layer_hom(Ctxt &out, Ctxt &in, const lowmc_t *lowmc, const EncryptedArray& ea) {

  ZZX mask0;
  ZZX mask1;
  ZZX mask2;
  ZZX maskr;

  const rci_t n = lowmc->n;
  const rci_t m = lowmc->m;

  for(rci_t i=n-3*m; i<n; i+=3) {
    ZZX tmp;
    ea.encodeUnitSelector(tmp, i+0);
    mask0 += tmp;
    ea.encodeUnitSelector(tmp, i+1);
    mask1 += tmp;
    ea.encodeUnitSelector(tmp, i+2);
    mask2 += tmp;
  }
  for(rci_t i=0; i<n-3*m; i++) {
    ZZX tmp;
    ea.encodeUnitSelector(tmp, i);
    maskr += tmp;
  }

  Ctxt x0 = Ctxt(in); x0.multByConstant(mask0); ea.rotate(x0, 0);
  printf("%2ld ", x0.findBaseLevel() );
  Ctxt x1 = Ctxt(in); x1.multByConstant(mask1); ea.rotate(x1, -1);
  Ctxt x2 = Ctxt(in); x2.multByConstant(mask2); ea.rotate(x2, -2);
  Ctxt xr = Ctxt(in); xr.multByConstant(maskr);

  Ctxt y0 = Ctxt(x1); y0.multiplyBy(x2); y0 += x0;
  Ctxt y1 = Ctxt(x0); y1.multiplyBy(x2); y1 += x0; y1 += x1;
  Ctxt y2 = Ctxt(x0); y2.multiplyBy(x1); y2 += x0; y2 += x1; y2 += x2;

  ea.rotate(y0, 0);
  ea.rotate(y1, 1);
  ea.rotate(y2, 2);

  out = Ctxt(y0);
  out += y1;
  out += y2;
  out += xr;
  assert(out.isCorrect());
}

void print_ctxt(Ctxt &ctxt, const EncryptedArray &ea, FHESecKey &secretKey, rci_t n) {
  std::vector<long> t;

  ea.decrypt(ctxt, secretKey, t);
  for(rci_t i=0; i<n; i++) {
    std::cout << t[i];
  }
  std::cout << "|";
  for(rci_t i=n; i<ea.size(); i++) {
    std::cout << t[i];
  }
  std::cout<< std::endl;
}


void ctxt_apply_linear(Ctxt &out, mzd_t *A, Ctxt &in, const EncryptedArray& ea, bool add) {
  assert(ea.size() >= A->nrows);
  const rci_t n = A->nrows;

  if (!add)
    out = Ctxt(in.getPubKey());

  vector<long> pmask;
  ZZX emask;
  pmask.resize(ea.size());

#if 1
  Ctxt lhs = in;
  Ctxt rhs = in;
  ea.rotate(rhs, n);
  lhs += rhs;

  for(rci_t i=0; i<n; i++) {
    for(rci_t j=0; j<n; j++) {
      pmask[j] = mzd_read_bit(A, j, (i+j) % n);
    }
    ea.encode(emask, pmask);
    Ctxt tmp = lhs;
    ea.rotate(tmp, -i);
    tmp.multByConstant(emask); // also kills all entries >= n
    out += tmp;
  }
#else
  for(rci_t i=0; i<n; i++) {
    for(rci_t j=0; j<n; j++) {
      pmask[j] = mzd_read_bit(A, j, (i+j) % n);
    }
    ea.encode(emask, pmask);
    Ctxt tmp = in;
    ea.rotate(tmp, -i); // we assume here that size() >= 2*n
    Ctxt rhs = in;
    ea.rotate(rhs, n-i); // we assume here that size() >= 2*n
    tmp += rhs;
    tmp.multByConstant(emask); // also kills all entries >= n
    out += tmp;
  }
#endif
}


void ctxt_add_constant(Ctxt &out, mzd_t *A, const EncryptedArray& ea) {
  ZZX tmp;
  ZZX r;
  for(rci_t i=0; i < A->nrows; i++) {
    if (mzd_read_bit(A, i, 0)) {
      ea.encodeUnitSelector(tmp, i);
      r += tmp;
    }
  }
  out.addConstant(r);
  assert(out.isCorrect() );
}

int main(int argc, char **argv) {
  int m,n,r,k,l,security, depth, B;
  parse_args(argc, argv, m, n, r, k, l, security, depth, B);

  if (l < 2*n)
    l = 2*n;

  long key_switch_ncols = 2; //< FIXME: how to choose this?

  srandom(time(NULL));

  setTimersOn();

  if (depth == 0)
    depth = r + ceil(log2(r));

  /*
    long FindM(long k, long L, long c, long p, long d, long s, long chosen m, bool verbose=false);

    In this method, k is the security parameter, L is the number of ciphertext-primes that we want
    to support, c is the number of columns in our key-switching matrices. The arguments p, d
    determine the plaintext space Fpd , the argument s bounds from below the number of plaintext
    slots that we want to support, and chosen m gives the ability to specify a particular m
    parameter and test if it satisfies all our constraints.  */

  long M = FindM(security, depth, key_switch_ncols, 2, 0, l, 0, true);

  printf("#rounds: %2d, #sboxes: %3d, n: %3d, lwe dimension: %5d",r, m, n, M);
  fflush(0);

  FHE_NTIMER_START(setup);

  FHEcontext context(M, 2, 1); //< initialise context
  context.bitsPerLevel = B;
  buildModChain(context, depth, key_switch_ncols); //< modify context, adding primes to modulus chain
  FHESecKey secretKey(context);
  const FHEPubKey& publicKey = secretKey; //< secret key is actually a subclass of publicKey
  ZZX G = context.alMod.getFactorsOverZZ()[0];
  secretKey.GenSecKey(security/2); //< hamming weight
  addSome1DMatrices(secretKey);

  EncryptedArray ea(context, G);
  long nslots = ea.size(); //< we don't control this
  printf(", nslots: %3d\n",nslots);

  FHE_NTIMER_STOP(setup);


  long failure = 0;

  lowmc_t *lowmc = lowmc_init(m, n, r, k);

  /* plaintext operations */

  mzd_t *p = mzd_init_random_vector(n);
  mzd_t *c = lowmc_call(lowmc, p);

  /* homomorphic operations */
  FHE_NTIMER_START(heenc);

  std::vector<long> key_p;
  std::vector<long> c_p;
  std::vector<long> p_p;

  std::vector<long> x_p;
  std::vector<long> y_p;
  std::vector<long> z_p;

  // set plaintext arrays
  for(long i=0; i<n; i++) {
    p_p.push_back(mzd_read_bit(p, i, 0));
    c_p.push_back(0);
  }

  for(long i=0; i<k; i++) {
    key_p.push_back(mzd_read_bit(lowmc->key, i, 0));
  }
  for(long i=k; i<n; i++) {
    key_p.push_back(0);
  }

  for(long i=n; i<nslots; i++) {
    p_p.push_back(0);
    key_p.push_back(0);
    c_p.push_back(0); //todo : do we need this?
  }

  // encrypt plaintext arrays
  Ctxt x_c = Ctxt(publicKey);
  Ctxt y_c = Ctxt(publicKey);
  ea.encrypt(x_c, publicKey, p_p);

  Ctxt key_c = Ctxt(publicKey);
  ea.encrypt(key_c, publicKey, key_p);

  FHE_NTIMER_STOP(heenc);
  FHE_NTIMER_START(run);

  FHE_NTIMER_START(key);
  ctxt_apply_linear(x_c, lowmc->KMatrix[0], key_c, ea, true);
  FHE_NTIMER_STOP(key);

  const FHEtimer *tp = getTimerByName("run");

  for(int i=0; i<r; i++) {
    printf("round %2d ", i);
    fflush(0);

    FHE_NTIMER_START(sbox);
    sbox_layer_hom(y_c, x_c, lowmc, ea);
    FHE_NTIMER_STOP(sbox);

    printf("s");
    fflush(0);

    ctxt_apply_linear(x_c, lowmc->LMatrix[i], y_c, ea, false);
    printf("l");
    fflush(0);

    ctxt_add_constant(x_c, lowmc->Constants[i], ea);
    printf("c");
    fflush(0);

    FHE_NTIMER_START(key);
    ctxt_apply_linear(x_c, lowmc->KMatrix[i+1], key_c, ea, true);
    FHE_NTIMER_STOP(key);
    printf("k");
    fflush(0);

    printf(" t: %7.2f s\n", (tp->getTime())/(i+1));
  }

  FHE_NTIMER_STOP(run);
  FHE_NTIMER_START(hedec);

  ea.decrypt(x_c, secretKey, c_p);
  for(int i=0; i<n; i++) {
    if (c_p[i] != mzd_read_bit(c, i, 0)) {
      std::cout << i << ": " << c_p[i] << mzd_read_bit(c, i, 0) << std::endl;
      failure = 1;
    }
  }


  mzd_free(p);
  mzd_free(c);

  FHE_NTIMER_STOP(hedec);

  printf("\n");
  setTimersOff();

  tp = getTimerByName("setup"); printf("setup: %5.2f s, ", tp->getTime());
  tp = getTimerByName("heenc"); printf("enc: %7.2f, ", tp->getTime());
  tp = getTimerByName("run");   printf("eval: %8.2f s ", tp->getTime());
  tp = getTimerByName("sbox");  printf("(sbox: %8.2f s, ", tp->getTime());
  tp = getTimerByName("key");   printf("key: %8.2f s), ", tp->getTime());
  tp = getTimerByName("hedec"); printf("dec: %7.2f s, ", tp->getTime());
  tp = getTimerByName("run");   printf("eval per block: %8.4f s, ", tp->getTime()/nslots);
                                printf("eval per bit: %8.4f s => ", tp->getTime()/nslots/n);



  if(!failure)
      printf("SUCCESS\n");
    else
      printf("FAILURE\n");

  return 0;
}
