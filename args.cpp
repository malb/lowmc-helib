#include "args.h"
#include "stdio.h"
#include "unistd.h"
#include "stdlib.h"

void print_header() {
  printf("----------------------------------------------------------------------\n");
  printf("| lowmc - 3-bit Sboxes and random matrices in HElib                  |\n");
  printf("----------------------------------------------------------------------\n");
}

void print_help() {
  print_header();
  printf("usage:\n");
  printf(" -m sbox number          integer > 0\n");
  printf(" -n block size           integer > 0\n");
  printf(" -r number of rounds     integer > 0\n");
  printf(" -d HELib depth          integer > 0\n");
  printf(" -k key bits             integer > 0\n");
  printf(" -l min. number of slots integer >= 0\n");
  printf(" -B bits per level       intrger > 0\n");
  printf(" -s he security          integer > 0\n");
}

int parse_args(int argc, char *argv[], int &m, int &n, int &r, int &k, int &l, int &security, int &depth, int &B) {
  m = 0;
  n = 0;
  r = 0;
  k = 0;
  l = 0;
  B = 0;
  depth = 0;
  security  = 0;

  int choice;
  while ((choice = getopt(argc, argv, "n:s:r:m:l:k:d:B:")) != -1)
    switch (choice) {
    case 'n':
      n = atoi(optarg);
      break;
    case 'm':
      m = atoi(optarg);
      break;
    case 'r':
      r = atoi(optarg);
      break;
    case 'k':
      k = atoi(optarg);
      break;
    case 'l':
      l = atoi(optarg);
      break;
    case 's':
      security = atoi(optarg);
      break;
    case 'd':
      depth = atoi(optarg);
      break;
    case 'B':
      B = atoi(optarg);
      break;
    case '?':
      print_help();
      abort();
    default:
      abort();
    }

  if (k == 0 || r == 0 || m == 0 || n < 3*m || security == 0 || B < 0) {
    print_help();
    abort();
  }

  return 0;
}
