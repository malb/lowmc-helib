HELIBDIR=`pwd`/helib/src
M4RIDIR=`pwd`/m4ri/

CXXOPTS=-ggdb -O3 -I$(HELIBDIR) -I$(M4RIDIR)
LDOPTS=-Wl,-rpath,$(HELIBDIR) -Wl,-rpath,$(M4RIDIR)/.libs $(HELIBDIR)/fhe.a -L$(M4RIDIR)/.libs -lm -lm4ri -lgmp -lntl

ifeq ($(NOTUNE),1)
	WIZARD="WIZARD=off"
else
	WIZARD=
endif

all:	lowmc-he lowmc-he-comp

lowmc-he: lowmc.cpp lowmc-he.cpp linear.cpp args.cpp helib/src/fhe.a m4ri/.libs/libm4ri.so
	$(CXX) $(CXXOPTS) lowmc-he.cpp lowmc.cpp linear.cpp args.cpp $(CXXOPTS) $(LDOPTS) -o lowmc-he

lowmc-he-comp: lowmc.cpp lowmc-he-comp.cpp linear.cpp args.cpp helib/src/fhe.a m4ri/.libs/libm4ri.so
	$(CXX) $(CXXOPTS) lowmc-he-comp.cpp lowmc.cpp linear.cpp args.cpp $(CXXOPTS) $(LDOPTS) -o lowmc-he-comp

matmul:	matmul.cpp helib/src/fhe.a
	$(CXX) $(CXXOPTS) matmul.cpp $(CXXOPTS) $(LDOPTS) -o matmul

helib/src/fhe.a: helib/src/*.cpp helib/src/*.h
	(cd $(HELIBDIR) && $(MAKE) $(MAKEOPTS))

m4ri/.libs/libm4ri.so: m4ri/m4ri/*.c m4ri/m4ri/*.h
	(cd $(M4RIDIR) && ./configure && $(MAKE) $(MAKEOPTS))

clean:
	rm -f matmul lowmc-he lowmc-he-comp
	(cd $(HELIBDIR) && make clean)
	(cd $(M4RIDIR) && make clean)

really-clean: clean
	(cd $(HELIBDIR) && git reset --hard)
