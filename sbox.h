#include <m4ri/m4ri.h>
#include <EncryptedArray.h>

void sbox_layer(mzd_t *out, mzd_t *in, rci_t m);
void sbox_layer_hom(Ctxt **out, Ctxt **in, const rci_t m);
