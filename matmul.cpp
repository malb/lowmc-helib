#include "timing.h"
#include "FHE.h"
#include "EncryptedArray.h"
#include <m4ri/m4ri.h>
#include <unistd.h>
#include <fstream>
#include <sstream>
#include <sys/time.h>

void print_header() {
  printf("----------------------------------------------------------------------\n");
  printf("| matmul - Matrix-Vector products in HElib                           |\n");
  printf("----------------------------------------------------------------------\n");
}


void print_help() {
  print_header();
  printf("usage:\n");
  printf(" -n dimension        integer > 0\n");
  printf(" -s he security      integer > 0\n");
};


int main(int argc, char *argv[]) {

  long n= 0;
  long security  = 0;

  int c;
  while ((c = getopt(argc, argv, "n:s:")) != -1)
    switch (c) {
    case 'n':
      n = atoll(optarg);
      break;
    case 's':
      security = atoll(optarg);
      break;
    case '?':
      print_help();
      abort();
    default:
      abort();
    }

  long key_switch_ncols = 2; //< FIXME: how to choose this?

  if (n == 0 || security == 0) {
    print_help();
    abort();
  }

  /*
    long FindM(long k, long L, long c, long p, long d, long s, long chosen m, bool verbose=false);

    In this method, k is the security parameter, L is the number of ciphertext-primes that we want
    to support, c is the number of columns in our key-switching matrices. The arguments p, d
    determine the plaintext space Fpd , the argument s bounds from below the number of plaintext
    slots that we want to support, and chosen m gives the ability to specify a particular m
    parameter and test if it satisfies all our constraints.  */

  setTimersOn();


  long m = FindM(security, 1, key_switch_ncols, 2, 0, 1, 0, 0);
  printf("n: %3ld, m: %5ld\n",n, m);

  FHE_NTIMER_START(setup);
  FHEcontext context(m, 2, 1); //< initialise context
  buildModChain(context, 1, key_switch_ncols); //< modify context, adding primes to modulus chain
  FHESecKey secretKey(context);
  const FHEPubKey& publicKey = secretKey; //< secret key is actually a subclass of publicKey

  ZZX G = context.alMod.getFactorsOverZZ()[0];

  secretKey.GenSecKey(security/2); //< hamming weight
  addSome1DMatrices(secretKey);

  FHE_NTIMER_STOP(setup);

  FHE_NTIMER_START(run);

  long failure = 0;

  mzd_t *A = mzd_init(n,n);
  mzd_t *v = mzd_init(n,1);
  mzd_t *w = mzd_init(n,1);

  EncryptedArray ea(context, G);

  long nslots = ea.size(); //< we don't control this
  printf("nslots: %3ld\n", nslots);

  const long ntrials = 16;
  for(long j=0; j<ntrials; j++) {

    mzd_randomize(A);
    mzd_randomize(v);
    mzd_set_ui(w, 0);
    mzd_mul(w, A, v, 0);

    std::vector<long> vp[n];
    std::vector<long> wp[n];
    // set plaintext arrays
    for(long i=0; i<n; i++) {
      for (long j=0; j<nslots; j++) {
        vp[i].push_back(mzd_read_bit(v, i, 0));
        wp[i].push_back(0);
      }
    }

    Ctxt *vc[n];
    Ctxt *wc[n];

    // encrypt plaintext arrays
    for(long i=0; i<n;i++) {
      vc[i] = new Ctxt(publicKey);
      ea.encrypt(*vc[i], publicKey, vp[i]);
    }

    for(long r=0; r<n; r++) {
      wc[r] = new Ctxt(publicKey);
      for(long c=0; c<n; c++)
        if (mzd_read_bit(A, r,c))
          *wc[r] += *vc[c];
    }

    for(long i=0; i<n; i++) {
      ea.decrypt(*wc[i], secretKey, wp[i]);
      if (wp[i][0] != mzd_read_bit(w, i, 0)) {
        failure++;
        break;
      }
      delete wc[i];
      delete vc[i];
    }
    if (failure)
      printf("-");
    else
      printf("+");
    fflush(0);
    failure = 0;
  }
  FHE_NTIMER_STOP(run);

  setTimersOff();
  printf("\n");

  FHEtimer setup(NULL,NULL);
  getTimerByName(setup, "setup");
  FHEtimer run(NULL,NULL);
  getTimerByName(run, "run");
  printf("setup time: %5.2f s, running time: %7.4f, ", setup.getTime(), (run.getTime()/nslots)/ntrials);

  if(!failure)
      printf("SUCCESS\n");
    else
      printf("FAILURE\n");


  return 0;
}
