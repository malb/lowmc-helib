#include <m4ri/m4ri.h>
#include <FHE.h>
#include <EncryptedArray.h>

bool getrandbit();

mzd_t *mzd_init_random_vector(rci_t n);
mzd_t *mzd_sample_lmatrix(rci_t n);
mzd_t *mzd_sample_kmatrix(rci_t n, rci_t k);
