#include <FHE.h>
#include <timing.h>
#include <EncryptedArray.h>
#include <unistd.h>
#include <fstream>
#include <sstream>
#include <sys/time.h>

#include "args.h"
#include "lowmc.h"
#include "linear.h"
#include "sbox.h"

void ctxt_copy(Ctxt **out, Ctxt **in, const rci_t ncols) {
  for(rci_t i=0; i<ncols; i++)
    *out[i] = Ctxt(*in[i]);
};

void ctxt_add(Ctxt **out, Ctxt **in, const rci_t ncols) {
  for(rci_t i=0; i<ncols; i++)
    *out[i] += *in[i];
};

void sbox_layer_hom(Ctxt **out, Ctxt **in, const rci_t m, const rci_t n, const EncryptedArray &ea) {
  for(rci_t i=n-3*m; i<n; i+=3) {
    Ctxt x0 = Ctxt(*in[i+0]);
    Ctxt x1 = Ctxt(*in[i+1]);
    Ctxt x2 = Ctxt(*in[i+2]);

    printf("%2ld ", x0.findBaseLevel() );
    fflush(0);

    Ctxt y0 = Ctxt(x1); y0.multiplyBy(x2); y0 += x0;
    Ctxt y1 = Ctxt(x0); y1.multiplyBy(x2); y1 += x0; y1 += x1;
    Ctxt y2 = Ctxt(x0); y2.multiplyBy(x1); y2 += x0; y2 += x1; y2 += x2;

    *out[i+0] = Ctxt(y0);
    *out[i+1] = Ctxt(y1);
    *out[i+2] = Ctxt(y2);
  }
  for(rci_t i=0; i<n-3*m; i++) {
    *out[i] = Ctxt(*in[i]);
  }
}

void ctxt_mul(Ctxt **W, mzd_t *A, Ctxt **V, const FHEPubKey &publicKey, bool add=false) {
  rci_t k = floor(log2(A->ncols)) - 2;
  if (k<1)
    k=1;
  rci_t k_store = k;

  rci_t *L = (rci_t*)m4ri_mm_calloc(sizeof(rci_t),1<<k);
  Ctxt **T = (Ctxt**)m4ri_mm_calloc(sizeof(Ctxt*),1<<k);

  if (!add) {
    for(rci_t i=0; i<A->nrows; i++) {
      *W[i] = Ctxt(publicKey);
    }
  }

  for(rci_t i=0; i<1<<k; i++)
    T[i] = new Ctxt(publicKey);

  L[0] = 0;
  for (rci_t i = 0; i < A->ncols; i+=k) {
    if (i+k > A->ncols)
      k = A->ncols - i;
    for (rci_t j = 1; j < 1<<k; j++) {
      rci_t rowneeded = i + m4ri_codebook[k]->inc[j - 1];
      L[m4ri_codebook[k]->ord[j]] = j;
      *T[j] = Ctxt(*V[rowneeded]);
      *T[j] += *T[j-1];
    }

    for(rci_t j = 0; j < A->nrows; j++) {
      rci_t x = L[ mzd_read_bits_int(A, j, i, k) ];
      *W[j] += *T[x];
    }
  }

  k = k_store;
  for(rci_t i=0; i<1<<k; i++)
    delete T[i];
  free(T);
  free(L);
}


void ctxt_add_constant(Ctxt **out, mzd_t *A, ZZX &one) {
  for(rci_t i=0; i < A->nrows; i++) {
    if (mzd_read_bit(A, i, 0))
      (*out[i]).addConstant(one);
    assert( (*out[i]).isCorrect() );
  }
}

int find_m(int security, int depth, int key_switch_ncols, int slots, int B) {
  int security_ = security/2;
  double actual_security = 0.0;
  long m = 0;
  while(actual_security < security) {
    m = FindM(security_, depth, key_switch_ncols, 2, 0, slots, 0, false);
    FHEcontext context(m, 2, 1); //< initialise context
    if (B) {
      context.bitsPerLevel = B;
    }
    buildModChain(context, depth, key_switch_ncols); //< modify context, adding primes to modulus chain
    actual_security = context.securityLevel();
    security_ += 4;
  }
  return m;
}

int main(int argc, char **argv) {
  int m,n,r,k,l, target_security, depth, B;
  parse_args(argc, argv, m, n, r, k, l, target_security, depth, B);

  if (l < m)
    l = m;

  long key_switch_ncols = 3; //< FIXME: how to choose this?

  srandom(time(NULL));

  setTimersOn();

  if (depth == 0)
    depth = r + ceil(log2(r)/2.0);

  double actual_security = 0.0;
  long nslots = 0;

  FHE_NTIMER_START(setup);

  long M = find_m(target_security, depth, key_switch_ncols, l, B);

  printf("r: %2d, m: %3d, n: %3d | B: %2d | LWE dim: %5ld, L: %2d, ",r, m, n, B, M, depth);
  fflush(0);

  FHEcontext context(M, 2, 1); //< initialise context

  if (B) {
    context.bitsPerLevel = B;
  }

  buildModChain(context, depth, key_switch_ncols); //< modify context, adding primes to modulus chain

  actual_security = context.securityLevel();

  IndexSet allPrimes(0, context.numPrimes()-1);
  printf("|p_i|: %ld (%ld, %ld), |∏ p_i|: %.1f, λ: (%3d, %3.0f), nslots: %4ld\n",
         context.numPrimes(),
         context.ctxtPrimes.card(),
         context.specialPrimes.card(),
         context.logOfProduct(allPrimes),
         target_security, actual_security,
         context.zMStar.getNSlots());

  FHESecKey secretKey(context);
  const FHEPubKey& publicKey = secretKey; //< secret key is actually a subclass of publicKey
  secretKey.GenSecKey(64); //< hamming weight
  addSome1DMatrices(secretKey);

  ZZX G = context.alMod.getFactorsOverZZ()[0];
  EncryptedArray ea(context, G);
  nslots = ea.size(); //< we don't control this

  FHE_NTIMER_STOP(setup);

  long failure = 0;

  lowmc_t *lowmc = lowmc_init(m, n, r, k);
  /* plaintext operations */

  mzd_t *p = mzd_init_random_vector(n);
  mzd_t *c = lowmc_call(lowmc, p);

  /* homomorphic operations */

  FHE_NTIMER_START(heenc);

  std::vector<long> key_p[k];
  std::vector<long> c_p[n];
  std::vector<long> p_p[n];

  std::vector<long> x_p[n];
  std::vector<long> y_p[n];
  std::vector<long> z_p[n];

  // set plaintext arrays
  for(long i=0; i<n; i++) {
    p_p[i].push_back(mzd_read_bit(p, i, 0));
    c_p[i].push_back(0);
    for (long j=1; j<nslots; j++) {
      p_p[i].push_back(0);
      c_p[i].push_back(0);
    }
  }

  for(long i=0; i<k; i++) {
    key_p[i].push_back(mzd_read_bit(lowmc->key, i, 0));
    for (long j=1; j<nslots; j++) {
      key_p[i].push_back(0);
    }
  }

  Ctxt *key_c[k];
  Ctxt *x_c[n];
  Ctxt *y_c[n];

  ZZX const1_poly;
  std::vector<long int> one;
  for(rci_t i=0; i<nslots; i++) {
    one.push_back(1);
  }
  ea.encode(const1_poly, one);

  // encrypt plaintext arrays
  for(long i=0; i<n;i++) {
    x_c[i] = new Ctxt(publicKey);
    y_c[i] = new Ctxt(publicKey);
    ea.encrypt(*x_c[i], publicKey, p_p[i]);
  }
  for(long i=0; i<k;i++) {
    key_c[i] = new Ctxt(publicKey);
    ea.encrypt(*key_c[i], publicKey, key_p[i]);
  }

  FHE_NTIMER_STOP(heenc);
  FHE_NTIMER_START(run);

  FHE_NTIMER_START(key);
  ctxt_mul(x_c, lowmc->KMatrix[0], key_c, publicKey, true);
  FHE_NTIMER_STOP(key);

  const FHEtimer *tp;

  for(int i=0; i<r; i++) {

    printf("round %2d ", i);
    fflush(0);

    FHE_NTIMER_START(sbox);
    sbox_layer_hom(y_c, x_c, m, n, ea);
    FHE_NTIMER_STOP(sbox);
    printf(".");
    fflush(0);

    ctxt_mul(x_c, lowmc->LMatrix[i], y_c, publicKey);
    printf(".");
    fflush(0);

    ctxt_add_constant(x_c, lowmc->Constants[i], const1_poly);
    printf(".");
    fflush(0);

    FHE_NTIMER_START(key);
    ctxt_mul(x_c, lowmc->KMatrix[i+1], key_c, publicKey, true);
    FHE_NTIMER_STOP(key);
    printf(".");
    fflush(0);


    printf("\n");
  }

  FHE_NTIMER_STOP(run);
  FHE_NTIMER_START(hedec);

  for(long i=0; i<n; i++) {
    ea.decrypt(*x_c[i], secretKey, c_p[i]);
    if (c_p[i][0] != mzd_read_bit(c, i, 0)) {
      std::cout << i << ": " << c_p[i][0] << mzd_read_bit(c, i, 0) << std::endl;
      failure = 1;
    }
    delete x_c[i];
    delete y_c[i];
  }

  for(long i=0; i<k; i++) {
    delete key_c[i];
  }
  mzd_free(p);
  mzd_free(c);

  FHE_NTIMER_STOP(hedec);

  setTimersOff();
  printf("\n");

  tp = getTimerByName("setup"); printf("setup: %5.2f s, ", tp->getTime());
  tp = getTimerByName("heenc"); printf("enc: %7.2f, ", tp->getTime());
  tp = getTimerByName("run");   printf("eval: %8.2f s ", tp->getTime());
  tp = getTimerByName("sbox");  printf("(sbox: %8.2f s, ", tp->getTime());
  tp = getTimerByName("key");   printf("key: %8.2f s), ", tp->getTime());
  tp = getTimerByName("hedec"); printf("dec: %7.2f s, ", tp->getTime());
  tp = getTimerByName("run");   printf("eval per block: %8.4f s, ", tp->getTime()/nslots);
                                printf("eval per bit: %8.4f s => ", tp->getTime()/nslots/n);


  if(!failure)
      printf("SUCCESS\n");
    else
      printf("FAILURE\n");

  return 0;
}
