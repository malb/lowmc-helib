#include "linear.h"
#include "lowmc.h"

void sbox_layer(mzd_t *out, mzd_t *in, rci_t m) {
  mzd_copy(out, in);
  for(rci_t n=out->nrows-3*m; n<out->nrows; n+=3) {
    word x0 = mzd_read_bit(in, n+0, 0);
    word x1 = mzd_read_bit(in, n+1, 0);
    word x2 = mzd_read_bit(in, n+2, 0);

    mzd_write_bit(out, n+0, 0, x1&x2 ^ x0);
    mzd_write_bit(out, n+1, 0, x0&x2 ^ x0 ^ x1);
    mzd_write_bit(out, n+2, 0, x0&x1 ^ x0 ^ x1 ^ x2);
  }
}

lowmc_t *lowmc_init(size_t m, size_t n, size_t r, size_t k) {
  lowmc_t *lowmc = (lowmc_t*)malloc(sizeof(lowmc_t));
  lowmc->m = m;
  lowmc->n = n;
  lowmc->r = r;
  lowmc->k = k;

  lowmc->LMatrix = (mzd_t**)calloc(sizeof(mzd_t*),r);
  for(int i=0; i<r; i++)
    lowmc->LMatrix[i] = mzd_sample_lmatrix(n);

  lowmc->Constants = (mzd_t**)calloc(sizeof(mzd_t*),r);
  for(int i=0; i<r; i++) {
    lowmc->Constants[i] = mzd_init_random_vector(n);
  }
  lowmc->KMatrix = (mzd_t**)calloc(sizeof(mzd_t*), r+1);
  for(int i=0; i<r+1; i++) {
    lowmc->KMatrix[i] = mzd_sample_kmatrix(n, k);
  }

  lowmc->key = mzd_init_random_vector(k);
  return lowmc;
}

mzd_t *lowmc_call(lowmc_t *lowmc, mzd_t *p) {
  mzd_t *c = mzd_init(lowmc->n,1);

  mzd_t *x = mzd_init(lowmc->n,1);
  mzd_t *y = mzd_init(lowmc->n,1);
  mzd_t *z = mzd_init(lowmc->n,1);

  mzd_copy(x, p);
  mzd_addmul(x, lowmc->KMatrix[0], lowmc->key, 0);

  for(int i=0; i<lowmc->r; i++) {
    sbox_layer(y, x, lowmc->m);
    mzd_mul(z, lowmc->LMatrix[i], y, 0);
    mzd_add(z, z, lowmc->Constants[i]);
    mzd_addmul(z, lowmc->KMatrix[i+1], lowmc->key, 0);
    mzd_copy(x, z);
  }
  mzd_copy(c, x);

  mzd_free(z);
  mzd_free(y);
  mzd_free(x);
  return c;
}

void lowmc_free(lowmc_t *lowmc) {
  for(int i=0; i<lowmc->r; i++) {
    mzd_free(lowmc->Constants[i]);
    mzd_free(lowmc->KMatrix[i]);
    mzd_free(lowmc->LMatrix[i]);
  }
  mzd_free(lowmc->KMatrix[lowmc->r]);
  free(lowmc->Constants);
  free(lowmc->LMatrix);
  free(lowmc->KMatrix);
  free(lowmc);
}
