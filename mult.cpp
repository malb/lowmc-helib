#include "timing.h"
#include "FHE.h"
#include "EncryptedArray.h"
#include <unistd.h>
#include <fstream>
#include <sstream>
#include <sys/time.h>

void print_header() {
  printf("----------------------------------------------------------------------\n");
  printf("| mult - Multiplication benchmark                                    |\n");
  printf("----------------------------------------------------------------------\n");
}


void print_help() {
  print_header();
  printf("usage:\n");
  printf(" -b blocksize        odd integer > 0\n");
  printf(" -s he security      integer > 0\n");
};


ZZX makeIrredPoly(long p, long d) {
  assert(d >= 1);
  assert(ProbPrime(p));

  if (d == 1) return ZZX(1, 1); // the monomial X

  zz_pBak bak; bak.save();
  zz_p::init(p);
  return to_ZZX(BuildIrred_zz_pX(d));
}


int main(int argc, char *argv[]) {

  long blocksize = 0;
  long security  = 0;

  int c;
  while ((c = getopt(argc, argv, "b:s:")) != -1)
    switch (c) {
    case 'b':
      blocksize = atoll(optarg);
      break;
    case 's':
      security = atoll(optarg);
      break;
    case '?':
      print_help();
      abort();
    default:
      abort();
    }

  long key_switch_ncols = 2; //< FIXME: how to choose this?

  if (blocksize == 0 || security == 0) {
    print_help();
    abort();
  };

  printf("b: %2d, ", blocksize);
  /*
    long FindM(long k, long L, long c, long p, long d, long s, long chosen m, bool verbose=false);

    In this method, k is the security parameter, L is the number of ciphertext-primes that we want
    to support, c is the number of columns in our key-switching matrices. The arguments p, d
    determine the plaintext space Fpd , the argument s bounds from below the number of plaintext
    slots that we want to support, and chosen m gives the ability to specify a particular m
    parameter and test if it satisfies all our constraints.  */

  setTimersOff();

  const long nrounds = 64;

  long m = FindM(security, nrounds, key_switch_ncols, 2, blocksize, 1, 0, 0);
  printf("m: %5d\n",m);

  startFHEtimer("setup");

  FHEcontext context(m, 2, 1); //< initialise context
  buildModChain(context, nrounds, key_switch_ncols); //< modify context, adding primes to modulus chain
  FHESecKey secretKey(context);
  const FHEPubKey& publicKey = secretKey; //< secret key is actually a subclass of publicKey
  
  ZZX G = makeIrredPoly(2, blocksize); 

  secretKey.GenSecKey(security/2); //< hamming weight
  addSome1DMatrices(secretKey);

  stopFHEtimer("setup");

  startFHEtimer("run");

  long nslots;
  long failure = 0;

  const long ntrials = 16;
  for(long j=0; j<ntrials; j++) {
    printf(".");
    fflush(0);

    EncryptedArray ea(context, G);
    nslots = ea.size(); //< we don't control this

    PlaintextArray p(ea);
    p.random();
    Ctxt cp(publicKey);
    ea.encrypt(cp, publicKey, p);

    PlaintextArray k(ea);
    k.random();
    Ctxt ck(publicKey);
    ea.encrypt(ck, publicKey, k);


    for(long i=0; i<nrounds; i++) {
      cp.multiplyBy(cp);
      p.mul(p);
      assert(cp.isCorrect());
    };
    PlaintextArray pp(ea);
    ea.decrypt(cp, secretKey, pp);

    if (!pp.equals(p)) 
      failure++;
  }
  stopFHEtimer("run");

  setTimersOff();
  printf("\n");

  printf("setup time: %5.2f s, running time: %7.4f, ", getTime4func("setup"), (getTime4func("run")/nslots)/ntrials);

  if(!failure)
      printf("SUCCESS\n");
    else
      printf("FAILURE\n");


  return 0;
}
